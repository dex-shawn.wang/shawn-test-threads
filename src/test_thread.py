# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""The Python implementation of the GRPC helloworld.Greeter server."""

from __future__ import print_function
from concurrent import futures
import logging
import time

import grpc
import helloworld_pb2
import helloworld_pb2_grpc

import logging

import grpc
import algo_pb2
import algo_pb2_grpc


def test_io_bound():
    logging.info("Will try to call algo ...")
    start_time = time.time()
    with grpc.insecure_channel('localhost:50052') as channel:
        stub = algo_pb2_grpc.AlgoStub(channel)
        response = stub.GetBestPlacement(algo_pb2.AlgoRequest(count=420))
    duration = time.time() - start_time
    logging.info(f"Algo client received: {response.message} duration {duration} s")

def test_cpu_bound():
    logging.info("Will try to call algo ...")
    start_time = time.time()
    result = Greeter.engage(421)
    duration = time.time() - start_time
    logging.info(f"Calculation result: {result} duration {duration} s")


def test_sleep_once():
    logging.info("Will sleep ...")
    start_time = time.time()
    time.sleep(2)
    duration = time.time() - start_time
    logging.info(f"Sleep duration {duration} s")

def test_sleep_for_loop():
    logging.info("Will sleep ...")
    start_time = time.time()
    for i in range(2000):
        time.sleep(0.001)
    duration = time.time() - start_time
    logging.info(f"Sleep duration {duration} s")

class Greeter(helloworld_pb2_grpc.GreeterServicer):

    @staticmethod
    def engage(count = 420):
        sum = 0
        mod = 999997
        start_time = time.time()
        for i in range(count):
            for j in range(count):
                for k in range(count):
                    sum = (sum + 1) % mod
        duration = time.time()-start_time
        logging.info(f"Greeter count {count} sum {sum} duration {duration} s")
        return sum

    def SayHello(self, request, context):
        logging.info(f"Request received, start computation for engagement")
        # Do Computation
        # sum = Greeter.engage()

        sum = 0
        test_sleep_for_loop()
        
        return helloworld_pb2.HelloReply(message='Hello, %d!' % sum)


def serve():
    port = '50051'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    helloworld_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port('[::]:' + port)
    server.start()
    logging.info("Server started, listening on " + port)
    return server

def main():
    server = serve()

    # test_cpu_bound()
    # test_io_bound()
    # test_sleep_once()
    test_sleep_for_loop()

    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s.%(msecs)03d %(levelname)s:\t%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    main()