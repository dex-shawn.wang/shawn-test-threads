from concurrent import futures
import logging
import time

import grpc
import algo_pb2
import algo_pb2_grpc


class Algo(algo_pb2_grpc.AlgoServicer):
    
    @staticmethod
    def compute(count = 500):
        sum = 0
        mod = 999997
        start_time = time.time()
        for i in range(count):
            for j in range(count):
                for k in range(count):
                    sum = (sum + 1) % mod
        duration = time.time()-start_time
        logging.info(f"count {count} sum {sum} duration {duration} s")
        return sum
    
    def GetBestPlacement(self, request, context):
        logging.info("Request GetBestPlacement received")
        count = request.count
        reply = Algo.compute(count)
        return algo_pb2.AlgoReply(message=reply)


def serve():
    port = '50052'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    algo_pb2_grpc.add_AlgoServicer_to_server(Algo(), server)
    server.add_insecure_port('[::]:' + port)
    server.start()
    logging.info("Algo server started, listening on " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s.%(msecs)03d %(levelname)s:\t%(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    serve()